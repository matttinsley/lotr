#Endearing Ents

##*Main Deck*

###Hero (3)
*  [Elrond](http://ringsdb.com/card/04128) _(Shadow and Flame)_
*  [Gandalf](http://ringsdb.com/card/142002) _(The Road Darkens)_
*  [Merry](http://ringsdb.com/card/10001) _(The Wastes of Eriador)_

###Ally (21)
* 2x [Beechbone](http://ringsdb.com/card/10118) _(The Battle of Carn Dûm)_
* 3x [Booming Ent](http://ringsdb.com/card/08141) _(The Antlered Crown)_
* 3x [Derndingle Warrior](http://ringsdb.com/card/10031) _(Escape from Mount Gram)_
* 2x [Quickbeam](http://ringsdb.com/card/143006) _(The Treason of Saruman)_
* 2x [Skinbark](http://ringsdb.com/card/144006) _(The Land of Shadow)_
* 3x [Treebeard](http://ringsdb.com/card/08146) _(The Antlered Crown)_
* 3x [Wandering Ent](http://ringsdb.com/card/08119) _(Celebrimbor's Secret)_
* 3x [Wellinghall Preserver](http://ringsdb.com/card/10061) _(Across the Ettenmoors)_

###Attachment (29)
* 3x [Ent Draught](http://ringsdb.com/card/143009) _(The Treason of Saruman)_
* 3x [Gandalf's Staff](http://ringsdb.com/card/142008) _(The Road Darkens)_
* 2x [Hobbit Pipe](http://ringsdb.com/card/141015) _(The Black Riders)_
* 2x [Hobbit Pony](http://ringsdb.com/card/10007) _(The Wastes of Eriador)_
* 3x [Narya](http://ringsdb.com/card/11015) _(The Grey Havens)_
* 3x [Secret Vigil](http://ringsdb.com/card/09012) _(The Lost Realm)_
* 2x [Self Preservation](http://ringsdb.com/card/01072) _(Core Set)_
* 3x [Shadowfax](http://ringsdb.com/card/143014) _(The Treason of Saruman)_
* 2x [Unexpected Courage](http://ringsdb.com/card/01057) _(Core Set)_
* 3x [Vilya](http://ringsdb.com/card/04137) _(Shadow and Flame)_
* 3x [Wizard Pipe](http://ringsdb.com/card/142009) _(The Road Darkens)_

###Event (9)
* 3x [Boomed and Trumpeted](http://ringsdb.com/card/10032) _(Escape from Mount Gram)_
* 3x [Entmoot](http://ringsdb.com/card/143011) _(The Treason of Saruman)_
* 3x [The Galadhrim's Greeting](http://ringsdb.com/card/01046) _(Core Set)_

3  Heroes,  59  Cards
Cards up to The Grey Havens

Deck built on [RingsDB](http://ringsdb.com).