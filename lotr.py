from flask import Flask
from flask import render_template
import model.game as game

app = Flask(__name__)
game.setup_game()
print(game.player.to_json())


@app.route('/')
def board():
    return render_template('board.html')


@app.route('/play')
def play():
    game.get_game_state()
    return render_template('board.html')


@app.route('/hand')
def hand():
    return game.player.hand.to_json()


@app.route('/card')
def card():
    return game.player.hand.cards[0].to_json()


@app.route('/next')
def do_next():
    return "?"

if __name__ == '__main__':
    app.run(debug=True)
    #game.start_round()

