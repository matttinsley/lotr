def when_revealed_effect(card):
    if card.pack_id == '1':
        core_set_when_revealed(card)
    else:
        print("Card reveal not implemented yet")


def core_set_when_revealed(card):
    print("Using when revealed card effect")
