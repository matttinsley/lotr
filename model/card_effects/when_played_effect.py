def when_played_effect(card):
    if card.pack_id == '1':
        core_set_when_played(card)
    else:
        print("Card effect not implemented yet")


def core_set_when_played(card):
    print("Using when played card effect")
