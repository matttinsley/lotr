from abc import ABC, abstractmethod
from enum import IntEnum
import json
from model.card_effects.when_played_effect import when_played_effect
from model.card_effects.when_revealed_effect import when_revealed_effect


class SphereType(IntEnum):
    Tactics = 1
    Spirit = 2
    Leadership = 3
    Lore = 4
    Neutral = 5
    Baggins = 6
    Fellowship = 7

    def __str__(self):
        return self.name


class CardType(IntEnum):
    Hero = 1
    Ally = 2
    Attachment = 3
    Event = 4
    Treasure = 5
    SideQuest = 6

    def __str__(self):
        return self.name


class Card(ABC):
    pack_id = ""
    type_id = ""
    type = ""
    sphere = ""
    position = -1
    code = ""
    cost = -1
    name = ""
    traits = ""
    text = ""
    is_unique = False
    threat = -1
    willpower = -1
    attack = -1
    defense = -1
    health = -1
    quantity = -1
    deck_limit = -1
    frontimgsrc = ""
    backimgsrc = ""
    resources_per_round = 0
    resources = 0
    num_in_deck = -1

    @abstractmethod
    def reveal(self):
        pass

    @abstractmethod
    def play(self):
        pass

    def to_json(self):
        json_dump = json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)
        clean_json = json_dump.replace("NaN", '""')
        return clean_json


class PlayerCard(Card):
    def __init__(self, card_dict):
        self.pack_id = card_dict['pack_id']
        self.type_id = card_dict['type_id']
        self.type = CardType(self.type_id)
        if self.type == CardType.Hero:
            self.resources_per_round = 1
        self.sphere = SphereType(card_dict['sphere_id'])
        self.position = card_dict['position']
        self.code = card_dict['code']
        self.cost = card_dict['cost']
        self.name = card_dict['name']
        self.traits = card_dict['traits']
        self.text = card_dict['text']
        self.is_unique = card_dict['is_unique']
        self.threat = card_dict['threat']
        self.willpower = card_dict['willpower']
        self.attack = card_dict['attack']
        self.defense = card_dict['defense']
        self.health = card_dict['health']
        self.quantity = card_dict['quantity']
        self.deck_limit = card_dict['deck_limit']
        front_img_id= card_dict['octgnid']
        self.frontimgsrc = "static/img/cards/" + front_img_id + ".jpg"
        self.backimgsrc = "static/img/player_card_back.jpg"

    def id(self):
        return self.code + '-' + self.num_in_deck.__str__()

    def reveal(self):
        when_revealed_effect(self)

    def play(self):
        when_played_effect(self)

    def add_round_resources(self):
        self.resources += self.resources_per_round

    def __str__(self):
        str = ""
        if self.is_unique: str += "*"
        str += self.name
        str += "  -  " + self.type.__str__()
        if self.type == CardType.Hero: str += " - resources: " + self.resources.__str__()
        str += " - id: " + self.id()
        return str

    def __eq__(self, other):
        """Override the default Equals behavior"""
        if isinstance(other, self.__class__):
            return self.code == other.code and self.num_in_deck == other.num_in_deck
        return NotImplemented

    def __ne__(self, other):
        """Define a non-equality test"""
        if isinstance(other, self.__class__):
            return not self.__eq__(other)
        return NotImplemented

    def __hash__(self):
        """Override the default hash behavior (that returns the id or the object)"""
        return hash(tuple(sorted(self.__dict__.items())))

