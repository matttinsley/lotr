import random
from enum import IntEnum
import json


class DeckType(IntEnum):
    DrawDeck = 1
    Hand = 2
    PlayerDiscardPile = 3
    Controlled = 4
    Encounter = 5
    EncounterDiscardPile = 6
    Quest = 7
    Heroes = 8

    def __str__(self):
        return self.name


class Deck:
    cards = []
    decktype = None
    num_cards_starting_hand = 6

    def shuffle(self):
        random.shuffle(self.cards)

    def draw(self):
        return self.cards.pop()

    def discard(self, i):
        return self.cards.pop(i)

    def discard_random(self):
        return self.cards.pop(random.randrange(len(self.cards)))

    def add(self, card):
        self.cards.append(card)

    def remove(self, card):
        self.cards.remove(card)

    def remove_type(self, card_type):
        removed_cards = [c for c in self.cards if c.type is card_type]
        self.cards[:] = [c for c in self.cards if c.type is not card_type]
        return removed_cards

    def get_type(self, card_type):
        selected_cards = []
        for card in self.cards:
            if card.type == card_type:
                selected_cards.append(card)
        return selected_cards

    def get_attr_sum(self, attribute):
        attr_sum = 0
        for card in self.cards:
            if card.__getattribute__(attribute):
                attr_sum += int(card.__getattribute__(attribute))
        return attr_sum

    def count(self):
        return len(self.cards)

    def __str__(self):
        deck_str = ""
        for card in self.cards:
            deck_str += card.__str__() + '\n'
        return deck_str

    def __init__(self, cards=None, deck=None, decktype=None):
        self.cards = []
        if cards:
            self.cards = cards
        if deck:
            self.cards = deck.cards
        self.decktype = decktype

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    def draw_hand(self):
        hand = []
        for i in range(self.num_cards_starting_hand):
            hand.append(self.draw())
        return hand

