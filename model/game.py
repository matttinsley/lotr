from enum import IntEnum
from .player import Player
from .encounter import Encounter
from .setup import import_all_player_cards


class Phase(IntEnum):
    Resource = 1
    Planning = 2
    Quest = 3
    Travel = 4
    Encounter = 5
    Combat = 6
    Refresh = 7

player = None
player_deck_name = 'ents'
encounter = None
round_num = 1
Phase = Phase.Resource


def setup_game():
    player_card_df = import_all_player_cards()
    global player
    player = Player()
    player.setup(player_deck_name, player_card_df)

    global encounter
    encounter = Encounter()
    encounter.setup()


def start_round():
    print(player)
    begin_resource_phase()
    action = wait_for_user()
    if action == 'next':
        start_round()
    if action == 'exit':
        return



def begin_resource_phase():
    player.add_round_resources()
    player.draw_card()


def wait_for_user():
    command = input('Enter: play  -  next  -  exit')
    if command == 'play':
        player.play_card(player.hand.cards[0], player.controlled.cards[0])
    return command



def get_game_state():
    hand = player.hand.to_json()
    controlled = player.controlled.to_json()

if __name__ == "__main__":
    setup_game()
