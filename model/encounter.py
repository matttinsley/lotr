from .decks import Deck, DeckType


class Encounter():
    quest_deck = None
    discard_pile = None
    encounter_deck = None

    def setup(self):
        # generic encounter setup
        self.quest_deck = Deck(decktype=DeckType.Quest)
        self.discard_pile = Deck(decktype=DeckType.EncounterDiscardPile)
        self.encounter_deck = Deck(decktype=DeckType.Encounter)

        # setup specific encounter

