import pandas as pd
import sqlite3
import re
import os.path
from .cards import PlayerCard
from .decks import Deck
from config import BASE_DIR


PLAYER_CARD_DB = 'data/card-data.db'


def import_all_player_cards():
    player_card_db_path = os.path.join(BASE_DIR, PLAYER_CARD_DB)
    db = sqlite3.connect(player_card_db_path)
    player_card_df = pd.read_sql("SELECT * FROM 'PlayerCard'", db)
    return player_card_df


def import_player_deck(card_df, name):
    if card_df.empty:
        print("Load player cards from db before loading player deck")
        return

    player_deck_path = os.path.join(BASE_DIR, 'data/player_decks/' + name + '.md')

    # pull card ids from markdown file (downloaded from ringsdb.com)
    card_ids = []
    with open(player_deck_path, 'r') as f:
        for line in f.readlines():
            quantity = 1
            quantity_match = re.search(r'([1-3])x', line)
            if quantity_match:
                quantity = int(quantity_match.group(1))
            card_match = re.search(r'card\/([0-9]*)', line)
            if card_match:
                for count in range(quantity):
                    card_ids.append(card_match.group(1))

    # select only cards in player's deck from all cards
    # there are more efficient ways to do this, but we need to allow
    # multiple copies of the same card to be added
    deck_df = pd.DataFrame()
    for card_id in card_ids:
        deck_df = deck_df.append(card_df.loc[card_df.code == card_id])

    # convert rows to cards and store in deck
    player_deck = Deck()
    for index, row in deck_df.iterrows():
        card_data = row.to_dict()
        card = PlayerCard(card_data)
        num_in_deck = [other.code for other in player_deck.cards].count(card.code)
        card.num_in_deck = num_in_deck + 1
        player_deck.add(card)

    return player_deck


if __name__ == '__main__':
    card_db = '../data/card-data.db'
    card_df = import_all_player_cards(card_db)
    deck = import_player_deck(card_df, 'ents')
