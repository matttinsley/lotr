from .decks import Deck, DeckType
from .setup import import_player_deck
from .cards import CardType, SphereType
import json


class Player:
    full_deck = None
    hand = None
    draw_deck = None
    discard_pile = None
    threat = 0
    controlled = None
    player_deck_name = ''

    def set_starting_threat(self):
        self.threat = self.controlled.get_attr_sum('threat')

    def setup(self, player_deck, all_player_cards):
        # load player deck
        self.player_deck_name = player_deck
        self.full_deck = import_player_deck(all_player_cards, self.player_deck_name)

        # heroes
        hero_cards = self.full_deck.remove_type(CardType.Hero)
        self.controlled = Deck(cards=hero_cards, decktype=DeckType.Controlled)
        self.set_starting_threat()

        # draw deck and hand
        self.draw_deck = Deck(deck=self.full_deck, decktype=DeckType.DrawDeck)
        self.draw_deck.shuffle()
        hand_cards = self.draw_deck.draw_hand()
        self.hand = Deck(cards=hand_cards, decktype=DeckType.Hand)
        self.discard_pile = Deck(decktype=DeckType.PlayerDiscardPile)

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    def add_round_resources(self):
        [card.add_round_resources() for card in self.controlled.cards]

    def draw_card(self):
        self.hand.add(self.draw_deck.draw())

    def play_card(self, card_to_play, card_to_pay, attach_to=None):
        if card_to_play not in self.hand.cards:
            print("Error: card being played not in hand")
            return
        if card_to_play.sphere == card_to_pay.sphere or card_to_play.sphere == SphereType.Neutral:
            print("Error: use correct sphere to pay for card")
            return
        if card_to_pay.resources < card_to_play.cost:
            print("Error: not enough resources to pay for card")
            return

        if card_to_play.type == CardType.Ally:
            self.controlled.add(card_to_play)
            card_to_play.play()
        elif card_to_play.type == CardType.Attachment:
            if not attach_to:
                print("Error: must specify card to attach to")
                return
            attach_to_idx = self.controlled.cards.index(attach_to)
            self.controlled.cards.index[attach_to_idx].attachments.append(card_to_play.id)
            self.controlled.add(card_to_play)
            card_to_play.play()
        elif card_to_play.type == CardType.Event:
            card_to_play.play()
            self.discard_pile.add(card_to_play)

        card_to_pay.resources -= card_to_play.cost
        self.hand.remove(card_to_play)


    def __str__(self):
        player_str = 'Player using ' + self.player_deck_name + ' deck\n'
        player_str += 'starting threat: ' + str(self.threat) + '\n'
        player_str += '\tControlled: \n' + self.controlled.__str__() + '\n'
        player_str += '\tHand: \n' + self.hand.__str__() + '\n'
        #player_str += '\tDraw Deck: \n' + self.draw_deck.__str__() + '\n'
        #player_str += '\tDiscard Pile: \n' + self.discard_pile.__str__() + '\n'
        return player_str
