import React from 'react';
import Card from './Card.jsx';

var staging_area =
{
    "cards" : [
        {
            "backimgsrc": "static/img/encounter_card_back.png",
            "frontimgsrc": "static/img/cards/0a0e6044-c5ab-43ed-832a-71516c7d035a.jpg"
        },
        {
            "backimgsrc": "static/img/encounter_card_back.png",
            "frontimgsrc": "static/img/cards/0a6774e7-f110-4917-84b5-1f81a7aefbf8.jpg"
        }
    ]
};

export default class StagingArea extends React.Component {
    render() {
        var { cards } = staging_area;
        
        var staging_style = {
            float: "right",
            margin: "auto",
            width: 70 + "%",
            position: "relative",
            "borderStyle": "solid",
            "textAlign": "center"
        };

        var card_components = cards.map((card, i) =>
            <Card key={i} card_data={card} />
        );

        return (
            <div className="controlled" style={staging_style}>
               Staging Area <br/>
               {card_components}
            </div>
        )
    }

}

