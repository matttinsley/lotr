import React from 'react';
import {Resource, Damage} from './Token.jsx';

export default class Card extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            faceup: true,
            hover: false
        };
    }

    loadCard() {
        /*
        this.serverRequest = $.get(this.props.source, function (result) {
            var json_result = JSON.parse(result);
            this.setState({
                frontimgsrc: json_result.frontimgsrc,
                backimgsrc: json_result.backimgsrc,
                resources: json_result.resources
            });
        }.bind(this));
        console.log(this);
        */
    }

    componentDidMount() {
        this.loadCard()
    }

    componentWillUnmount() {
        this.serverRequest.abort();
    }

    render() {
        var { card_data, style } = this.props;
        var { faceup, hover } = this.state;
        
        var long_side = 220;
        var short_side = 150;
        var border_radius = 10;

        var background_img = faceup ? card_data.frontimgsrc : card_data.backimgsrc;
        var width = card_data.landscape ? long_side : short_side;
        var height = card_data.landscape ? short_side : long_side;

        var card_style = {
            position: "relative",
            left: 0,
            width: width,
            height: height,
            float: "left",
            bottom: 0,
            borderRadius: border_radius,
            transition: ".5s ease"
        };
        for (var attrname in style) { card_style[attrname] = style[attrname]; }

        var resource_components = [];
        var damage_components = [];
        if(faceup){
            for (var i=0; i<card_data.resources; i++){
                resource_components.push(<Resource key={i} number={i} card_width={card_style.width}/>);
            }
            for (var i=0; i<card_data.damage; i++){
                damage_components.push(<Damage key={i} number={i} card_width={card_style.width}/>);
            }
        }
        
        if(hover){
            var hover_style = this.setHoverStyle();
            for (var attrname in hover_style) { card_style[attrname] = hover_style[attrname]; }
        }
        
        return (
            <div className="card" style={card_style}  onDoubleClick={this.flipCard.bind(this)} onMouseEnter={this.onMouseChange.bind(this)} onMouseLeave={this.onMouseChange.bind(this)} >
                <img className="card-image" style={card_style} src={background_img} />
                {damage_components}
                {resource_components}
            </div>
        )
    }
    
    flipCard() {
        this.setState({faceup: !this.state.faceup});
    }

    onMouseChange(e) {
        this.setState({
            hover: !this.state.hover,
            mouseX: e.clientX,
            mouseY: e.clientY
        });
    }
    
    setHoverStyle(){
        var { mouseX, mouseY} = this.state;
        var hover_style = {
            WebkitTransform: "scale(1.4)",
            msTransform: "scale(1.4)",
            transform: "scale(1.4)",
            transition: ".5s ease",
            transformOrigin: "",
            zIndex: 200
        };

        if(mouseX < screen.width/5){
            hover_style.transformOrigin += "left ";
        } else if(mouseX > screen.width - screen.width/5) {
            hover_style.transformOrigin += "right ";
        }

        if(mouseY < screen.height/3){
            hover_style.transformOrigin += "top";
        } else if(mouseY > screen.height - screen.height/3) {
            hover_style.transformOrigin += "bottom";
        }
        return hover_style;
    }
}
Card.defaultProps = {
  landscape: false
};

