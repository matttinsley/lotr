import React from 'react';
import Card from './Card.jsx';

export default class Controlled extends React.Component {
    render() {
        var { cards } = this.props;
        
        var controlled_style = {
            bottom: 0,
            position: "absolute",
            width: 80 + "%",
            "borderStyle": "solid",
            "textAlign": "center"
        };

        var controlled_card_style = {
            bottom: 0
        };
        var card_components = cards.map((card, i) =>
            <Card key={i} card_data={card} style={controlled_card_style}/>
        );

        return (
            <div className="controlled" style={controlled_style}>
                Controlled<br/>
               {card_components}
            </div>
        )
    }

}

