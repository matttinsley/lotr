import React from 'react';
import Card from './Card.jsx';

export default class DrawDeck extends React.Component {
    render() {
        var div_style = {
            bottom: 0,
            right: 0,
            position: "absolute",
            float: "right",
            "borderStyle": "solid",
            "textAlign": "center"
        };

        var card_data = {
            backimgsrc: 'static/img/player_card_back.jpg',
            frontimgsrc: 'static/img/player_card_back.jpg'
        };
        return (
            <div className="drawdeck" style={div_style}>
                Draw Deck <br/>
                <Card card_data={card_data} />
            </div>
        )
    }
}
