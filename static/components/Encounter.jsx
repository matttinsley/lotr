import React from 'react';
import Quest from '../../static/components/Quest.jsx';
import ActiveLocation from '../../static/components/ActiveLocation.jsx';
import StagingArea from '../../static/components/StagingArea.jsx';

export default class Encounter extends React.Component {
    
    render() {
        var div_style = {
            top: 0,
            textAlign: "center",
            margin: "auto",
            position: "relative",
            height: 50 + "%"
        };
        
        return (
            <div style={div_style}>
                Encounter <br/>
                <Quest />
                <ActiveLocation />
                <StagingArea />
            </div>
        )
    }
}
