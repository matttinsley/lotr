import React from 'react';
import Card from './Card.jsx';

export default class ActiveLocation extends React.Component {
    render() {
        var card_data = {
            backimgsrc: 'static/img/cards/0bd34136-ae3d-44aa-b48f-7190c14f4b3b.jpg',
            frontimgsrc: 'static/img/cards/0bd34136-ae3d-44aa-b48f-7190c14f4b3b.jpg'
        };
        var div_style = {
            float: "left"
        };
        return (
            <div style={div_style} >
                Active Location<br/>
                <Card card_data={card_data} />
            </div>
        )
    }
}
