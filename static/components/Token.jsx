import React from 'react';

export class Token extends React.Component {
    render() {
        var { type, imgsrc, number, init_top_pos, card_width } = this.props;
        var offset_increment = 30;
        var offset = offset_increment * number;
        var init_left_pos = 60;

        var token_style = {
            position: "absolute",
            top: init_top_pos,
            left: init_left_pos + offset,
            width: 50,
            height: 50,
            userDrag: "none",
            userSelect: "none",
            MozUserDrag: "none",
            WebkitUserDrag: "none",
            msUserDrag: "none",
            MozUserSelect: "none",
            WebkitUserSelect: "none",
            msUserSelect: "none"
        };
        if(init_left_pos + offset + token_style.width > card_width){
            token_style.top += offset_increment;
            token_style.left = init_left_pos;
        }
        return (
            <img className={type} src={imgsrc} style={token_style}/>
        )
    }

}

export class Resource extends Token {}
Resource.defaultProps = {
    type: 'resource',
    imgsrc: 'static/img/tokens/resource.png',
    init_top_pos: 100
};

export class Damage extends Token {}
Damage.defaultProps = {
    type: 'damage',
    imgsrc: 'static/img/tokens/damage.png',
    init_top_pos: 60
};

export class Progress extends Token {}
Progress.defaultProps = {
    type: 'progress',
    imgsrc: 'static/img/tokens/progress.png',
    init_top_pos: 20
};
