import React from 'react';
import Card from './Card.jsx';

export default class Quest extends React.Component {
    render() {

        var card_data = {
            backimgsrc: 'static/img/cards/0ae0d2d5-b082-4cc7-b227-9b60c60de8b4.B.jpg',
            frontimgsrc: 'static/img/cards/0ae0d2d5-b082-4cc7-b227-9b60c60de8b4.jpg',
            landscape: true
        };
        var div_style = {
            float: "left"
        };
        return (
            <div style={div_style} >
                Quest <br/>
                <Card card_data={card_data} />
            </div>
        )
    }
}
